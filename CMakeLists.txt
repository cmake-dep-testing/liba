cmake_minimum_required(VERSION 3.0.0)
project(liba VERSION 1.0.0)

add_subdirectory(extern/libd)

add_library(a STATIC src/a.cpp)
target_include_directories(a PUBLIC include)
target_link_libraries(a PRIVATE d::d)
add_library(a::a ALIAS a)
