
#include <iostream>
#include "a/a.h"
#include "d/d.h"

namespace a {
  void addIndentation(uint indentation) {
    for (uint i = 0; i < indentation; ++i) {
      std::cout << "  ";
    }
  }

  void doThingA(uint indentation) {
    addIndentation(indentation);
    std::cout << "A v1" << std::endl;

    d::doThingD(indentation + 1);
  };
}
